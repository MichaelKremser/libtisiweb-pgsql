﻿using mkcs.libtisiweb.Data.Entities;

namespace mkcs.libtisiweb.Data.Access.Guestbook;

public class PostgreSqlGuestbookDataAccess : IGuestbookDataAccess
{
    public IEnumerable<GuestbookEntry> GetEntries(Guid siteID, int offset, int count)
    {
        throw new NotImplementedException();
    }

    public int GetCount(Guid siteID)
    {
        throw new NotImplementedException();
    }

    public void DeleteEntry(Guid id, bool softDelete = true)
    {
        throw new NotImplementedException();
    }

    public bool UpdateEntry(GuestbookEntry guestbookEntry)
    {
        throw new NotImplementedException();
    }
}
