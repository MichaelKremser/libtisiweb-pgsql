using mkcs.libtisiweb.Data.Access.Guestbook;

namespace mkcs.libtisiwebpgsqltests.UnitTests;

public class PostgreSqlGuestbookDataAccessTests
{
    [Fact]
    public void Ctor_NoArguments_CreatesObject()
    {
        // Act
        var uut = new PostgreSqlGuestbookDataAccess();

        // Assert
        Assert.NotNull(uut);
    }
}